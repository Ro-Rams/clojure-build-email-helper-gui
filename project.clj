(defproject send-build-email-gui "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [seesaw "1.4.5"]
                 [com.draines/postal "2.0.0"]
                 [javax.mail/javax.mail-api "1.5.5"]]
  :main send-build-email-gui.core)
