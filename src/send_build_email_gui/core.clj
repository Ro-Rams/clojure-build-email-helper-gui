(ns send-build-email-gui.core
  (:gen-class :main true)
  (:use seesaw.core
        postal.core
        seesaw.dev))

(defn read-recipents-file [filename] (clojure.string/split (slurp filename) #" "))
(def user "rory.armstrong@almacgroup.com")
(def tempez-recipents (read-recipents-file "tempez-recipents.txt"))
(def connect-recipents (read-recipents-file "connect-recipents.txt"))
(def host (read-recipents-file "host-name.text"))
(def conn {:host host
           :port 25})

(defn send-test-message [val] (send-message conn {:from user
                                                  :to user
                                                  :subject val
                                                  :body val}))

(defn send-tempez-deploy-message [] (send-message conn {:from user
                                                        :to tempez-recipents
                                                        :subject "Tempez development build"
                                                        :body "Hi all,\n\nRunning a development build in ten minutes.\n\nThanks,\nRory"}))

(defn send-tempez-deploy-complete-message [] (send-message conn {:from user
                                                                 :to tempez-recipents
                                                                 :subject "Tempez development build complete"
                                                                 :body "Hi all,\n\nDevelopment build complete\n\nRory"}))

(defn send-connect-deploy-message [] (send-message conn {:from user
                                                         :to connect-recipents
                                                         :subject "Tempez Connect development build"
                                                         :body "Hi all,\n\nRunning a quick development build.\n\nThanks,\nRory"}))

(defn send-connect-deploy-complete-message [] (send-message conn {:from user
                                                                  :to connect-recipents
                                                                  :subject "Tempez Connect development build complete"
                                                                  :body "Hi all,\n\nDevelopment build complete.\n\nRory"}))


(def button-size [300 :by 50])

(def tempez-build-button (button :id :tempez-deploy
                                 :text "Send Tempez DEPLOY email"
                                 :size button-size))
(def tempez-build-complete-button (button :id :tempez-deploy-complete
                                          :text "Send Tempez DEPLOY COMPLETE email"
                                          :size button-size
                                          :enabled? false))

(def tempez-connect-build-button (button :id :connect-deploy 
                                         :text "Send Tempez-Connect DEPLOY email"
                                         :size button-size))

(def tempez-connect-build-complete-button (button :id :connect-deploy-complete 
                                                   :text "Send Tempez-Connect DEPLOY COMPLETE email"
                                                   :size button-size
                                                   :enabled? false))

(def tempez-buttons (top-bottom-split tempez-build-button tempez-build-complete-button))

(def connect-buttons (top-bottom-split tempez-connect-build-button tempez-connect-build-complete-button))

(defn change-enabled-value-for-build-buttons 
  [bool] 
  (config! tempez-build-button :enabled? bool)
  (config! tempez-connect-build-button :enabled? bool))

(defn add-behaviours [root]
  (listen (select root [:#tempez-deploy]) 
          :action (fn [e] 
                    (send-tempez-deploy-message) 
                    (config! tempez-build-complete-button :enabled? true) 
                    (change-enabled-value-for-build-buttons false)))
  (listen (select root [:#tempez-deploy-complete]) 
          :action (fn [e] 
                    (send-tempez-deploy-complete-message)
                    (change-enabled-value-for-build-buttons true)
                    (config! tempez-build-complete-button :enabled? false)))
  (listen (select root [:#connect-deploy]) 
          :action (fn [e] 
                    (send-connect-deploy-message)
                    (config! tempez-connect-build-complete-button :enabled? true)
                    (change-enabled-value-for-build-buttons false)))
  (listen (select root [:#connect-deploy-complete]) 
          :action (fn [e] 
                    (send-connect-deploy-complete-message)
                    (change-enabled-value-for-build-buttons true)
                    (config! tempez-connect-build-complete-button :enabled? false)))
  root)

(def deploy-buttons (flow-panel :align :left :hgap 20 :items [tempez-buttons connect-buttons])) 

(defn make-frame []
  (native!)
  (frame :title "Deployment email helper"
         :width 700
         :height 200
         :content deploy-buttons))

(defn -main [& args]
    (-> (make-frame)
      add-behaviours
      show!))